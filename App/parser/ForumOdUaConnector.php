<?php

namespace App\parser;

use App\database\Connection;
use App\kernel\Task;
use App\parser\contracts\SiteConnector;
use Symfony\Component\DomCrawler\Crawler;

class ForumOdUaConnector extends Connector implements SiteConnector
{

    /**
     * ForumOdUaConnector constructor.
     */
    public function __construct()
    {
        $this->configPath = 'app.sites.forumodua.';
        parent::__construct();
    }

    /**
     *  Login
     */
    public function logIn(): void
    {
        if (!$this->isAuthorized()) {
            $this->clearCookies();
            $post = $this->config->getValue($this->configPath . 'loginPostData');
            $this->request($this->baseUrl . $this->config->getValue($this->configPath . 'loginUrl'), $post);
        }
    }

    /**
     * Logout
     */
    public function logOut(): void
    {
        $this->clearCookies();
    }

    /**
     * Parse a page
     * @param string $url
     * @return string
     */
    public function getPage(string $url): string
    {
        return $this->request($url);
    }

    /**
     * Save parsed data
     * @param $data
     * @return void
     */
    public function save($data): void
    {
        $task = new Task();

        foreach ($data as $post) {
            $task->queue('task_queue', $post);
        }
        $task->disconnect();
    }

    /**
     * @return bool
     */
    protected function isAuthorized(): bool
    {
        $page = $this->request($this->config->getValue($this->configPath . 'baseUrl'));
        $document = new Crawler($page);
        try {
            $username = trim($document->filter($this->config->getValue($this->configPath . 'selectors.login'))->text());
        } catch (\Exception $exception) {
            $username = null;
        }

        return $username === $this->config->getValue($this->configPath . 'login');
    }
}
