<?php

namespace App\parser\contracts;

interface SiteConnector
{
    /**
     *  Login
     */
    public function logIn(): void;

    /**
     * Logout
     */
    public function logOut(): void;

    /**
     * Parse a page
     * @param string $url
     * @return string
     */
    public function getPage(string $url): string;
}