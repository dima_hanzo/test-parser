<?php

namespace App\parser;

use App\parser\contracts\SiteConnector;
use Symfony\Component\DomCrawler\Crawler;

class ForumOdUaParser extends SiteParser
{
    const CONFIG_KEY = 'app.sites.forumodua.';

    /**
     * @var int
     */
    private $page = 0;

    /**
     * @param string $url
     * @param bool $forceLogout
     * @throws \Exception
     */
    public function parse(string $url, bool $forceLogout = false): void
    {
        $connector = $this->getConnector();
        if ($forceLogout) {
            $connector->logout();
        }
        $connector->logIn();
        $timeout = $this->config->getValue(self::CONFIG_KEY . 'timeout');
        do {
            $this->data = $connector->getPage($this->prepareLink($url));
            $posts = $this->parsePage();
            $connector->save($posts);
            $this->page++;
            sleep($timeout);
        } while ($this->config->getValue(self::CONFIG_KEY . 'limit') >= $this->page - 1);

        if ($forceLogout) {
            $connector->logout();
        }
    }

    /**
     * @return SiteConnector
     */
    protected function getConnector(): SiteConnector
    {
        return new ForumOdUaConnector();
    }

    /**
     * @return array
     * @throws \Exception
     */
    protected function parsePage(): array
    {
        $result = [];
        $crawler = new Crawler($this->data);
        $thread = trim($crawler->filter($this->config->getValue(self::CONFIG_KEY . 'selectors.thread'))->text());
        $posts = $crawler->filter($this->config->getValue(self::CONFIG_KEY . 'selectors.posts'));
        foreach ($posts as $post) {
            /** @var \DOMElement $post */
            $postAttrIds = explode('_', $post->getAttribute('id'));
            if ($this->isPost($postAttrIds)) {
                $result[$this->getPostId($postAttrIds)] = $this->getPostData($thread, $post);
            }
        }

        return $result;
    }

    /**
     * @param array $postAttrIds
     * @return bool
     */
    private function isPost(array $postAttrIds): bool
    {
        return reset($postAttrIds) === $this->config->getValue(self::CONFIG_KEY . 'selectors.postAttr');
    }

    /**
     * @param array $postAttrIds
     * @return string
     */
    private function getPostId(array $postAttrIds): string
    {
        return end($postAttrIds);
    }

    /**
     * @param string $thread
     * @param $nodes
     * @return array
     * @throws \Exception
     */
    private function getPostData(string $thread, $nodes): array
    {
        $post = new Crawler($nodes->childNodes);
        $result['title'] = $thread;
        $result['date'] = $this->prepareDateTime(trim($post->filter($this->config->getValue(self::CONFIG_KEY . 'selectors.date'))->text()));
        $result['author'] = trim($post->filter($this->config->getValue(self::CONFIG_KEY . 'selectors.author'))->text());
        $result['text'] = trim($post->filter($this->config->getValue(self::CONFIG_KEY . 'selectors.text'))->text());

        return $result;
    }

    /**
     * @param string $dateTime
     * @return string
     * @throws \Exception
     */
    private function prepareDateTime(string $dateTime): string
    {
        $dateTime = preg_replace('/[\x00-\x1F\x7F\xA0]/u', ' ', $dateTime);

        $date = new \DateTime();
        return $date::createFromFormat(
            $this->config->getValue(self::CONFIG_KEY . 'dateFormat'),
            $dateTime)->format($this->config->getValue('app.datetimeFormat')
        );
    }

    private function prepareLink(string $url):string
    {
        $link = explode('&page=', $url);
        $page = count($link) > 1 ? end($link) + $this->page : $this->page + 1;
        return reset($link) . "&page=$page";
    }

}