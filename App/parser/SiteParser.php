<?php

namespace App\parser;

use App\kernel\Config;
use App\parser\contracts\SiteConnector;

abstract class SiteParser
{
    /**
     * @var string $data
     */
    protected $data;

    /**
     * @var Config
     */
    protected $config;

    /**
     * SiteParser constructor.
     */
    public function __construct()
    {
        $this->config = Config::getInstance();
    }

    /**
     * @param string $url
     * @param bool $forceLogout
     */
    abstract public function parse(string $url, bool $forceLogout = false): void;

    /**
     * @return SiteConnector
     */
    abstract protected function getConnector(): SiteConnector;

    /**
     * @return array
     */
    abstract protected function parsePage(): array;

}