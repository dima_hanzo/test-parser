<?php

namespace App\parser;

use App\kernel\Config;

abstract class Connector
{
    /**
     * @var Config $config
     */
    protected $config;

    /**
     * @var string $baseUrl
     */
    protected $baseUrl;

    /**
     * @var string $configPath
     */
    protected $configPath;

    /**
     * @var bool|string
     */

    /**
     * Connector constructor.
     */
    public function __construct()
    {
        $this->config = Config::getInstance();
        $this->baseUrl = $this->config->getValue($this->configPath . 'baseUrl');
    }

    /**
     * @param string $url
     * @param array|null $post
     * @return bool|string
     */
    final protected function request(string $url, ?array $post = null)
    {
        $ch = curl_init($url);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_USERAGENT,
            'Mozilla/5.0 (Windows NT 6.2) AppleWebKit/536.6 (KHTML, like Gecko) Chrome/20.0.1090.0 Safari/536.6');
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        if ($this->config->existValue($this->configPath . 'cookiePath')) {
            $cookiePath = $this->config->getValue($this->configPath . 'cookiePath');
            curl_setopt($ch, CURLOPT_COOKIEJAR, realpath($cookiePath));
            curl_setopt($ch, CURLOPT_COOKIEFILE, realpath($cookiePath));
        }

        if ($post) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        }

        $html = curl_exec($ch);

        curl_close($ch);

        return $html;
    }

    /**
     * Clear cookies
     */
    final protected function clearCookies(): void
    {
        file_put_contents(realpath($this->config->getValue($this->configPath . 'cookiePath')), '');
    }

    /**
     * Checking if authorized
     * @return bool
     */
    abstract protected function isAuthorized(): bool;
}