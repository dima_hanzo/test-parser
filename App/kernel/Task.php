<?php

namespace App\kernel;

use App\rabbit\Connection;

class Task extends Connection
{
    /**
     * @param string $queue
     * @param $data
     */
    public function queue(string $queue, $data): void
    {
        $this->channel->queue_declare($queue, false, true, false, false);

        $this->sendMessage($queue, $data);
    }
}