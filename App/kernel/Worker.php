<?php

namespace App\kernel;

use App\database\Connection as ConnectionDB;
use App\rabbit\Connection;

class Worker extends Connection
{
    /**
     * @param string $queue
     * @throws \ErrorException
     */
    public function listen(string $queue): void
    {
        $this->channel->queue_declare($queue, false, true, false, false);

        echo " [*] Waiting for messages. To exit press CTRL+C\n";

        $callback = function ($msg) {
            echo ' [x] Received ', $this->decodeMessage($msg->body), "\n";
            echo " [x] Done\n";
            $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
            $this->saveToDB($this->decodeMessage($msg->body));
        };


        $this->channel->basic_qos(null, 1, null);
        $this->channel->basic_consume($queue, '', false, false, false, false, $callback);

        while (count($this->channel->callbacks)) {
            $this->channel->wait();
        }

        $this->disconnect();
    }

    /**
     * @param array $data
     */
    private function saveToDB(array $data): void
    {
        $message = "Something went wrong! \n";
        /** @var \PDO $db */
        $db = ConnectionDB::getInstance()->db();
        $query = $db->prepare("CALL insertpost (:title, :author, :date, :text);");
        if ($query->execute($data)) {
            $message = "Successfully saved! \n";
        }
        echo $message;
    }
}