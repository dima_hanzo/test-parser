<?php

namespace App\database;

use App\kernel\Config;
use App\pattern\singleton\Singleton;
use PDO;
use PDOException;

class Connection extends Singleton
{
    /**
     * @var PDO
     */
    public $connection;

    /**
     * @var string
     */
    private $credentials;

    /**
     * @var Config
     */
    private $config;

    /**
     * Connection constructor.
     */
    protected function __construct()
    {
        parent::__construct();
        $this->config = $config = Config::getInstance();
        $this->credentials = $this->prepareDSN();
        try {
            $this->connection = new PDO($this->credentials);
            $this->connection->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    /**
     * @return PDO
     */
    public function db()
    {
        return $this->connection;
    }

    /**
     * @return string
     */
    private function prepareDSN(): string
    {
        $defaultDB = $this->config->getValue('app.db');
        $prefix = $this->config->getValue("database.$defaultDB.prefix");
        $host = $this->config->getValue("database.$defaultDB.host");
        $port = $this->config->getValue("database.$defaultDB.port");
        $db = $this->config->getValue("database.$defaultDB.database");
        $user = $this->config->getValue("database.$defaultDB.user");
        $password = $this->config->getValue("database.$defaultDB.password");

        return "$prefix:host=$host;port=$port;dbname=$db;user=$user;password=$password";
    }
}