<?php

namespace App\rabbit;

use App\kernel\Config;
use App\pattern\singleton\Singleton;
use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class Connection extends Singleton
{
    const CONFIG_KEY_RABBITMQ = 'rabbitmq.';

    /**
     * @var Config $config
     */
    protected $config;

    /**
     * @var AMQPStreamConnection
     */
    protected $connection;

    /**
     * @var AMQPChannel
     */
    protected $channel;

    public function __construct()
    {
        /** @var Config $config */
        $this->config = Config::getInstance();
        $this->connect();
    }

    /**
     * Connection
     */
    protected function connect(): void
    {
        $this->connection = new AMQPStreamConnection(
            $this->config->getValue(self::CONFIG_KEY_RABBITMQ . 'host'),
            $this->config->getValue(self::CONFIG_KEY_RABBITMQ . 'port'),
            $this->config->getValue(self::CONFIG_KEY_RABBITMQ . 'user'),
            $this->config->getValue(self::CONFIG_KEY_RABBITMQ . 'password')
        );
        $this->channel = $this->connection->channel();
    }

    /**
     * Disconnect
     */
    public function disconnect(): void
    {
        $this->channel->close();
        $this->connection->close();
    }

    /**
     * @param string $queue
     * @param $data
     */
    protected function sendMessage(string $queue, $data): void
    {
        $message = new AMQPMessage(
            $this->encodeMessage($data),
            array('delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT)
        );

        $this->channel->basic_publish($message, '', $queue);

        echo ' [x] Sent ', $data, "\n";
    }

    /**
     * @param array $data
     * @return string
     */
    protected function encodeMessage($data): string
    {
        return json_encode($data);
    }

    /**
     * @param string $message
     * @return array
     */
    protected function decodeMessage(string $message)
    {
        return json_decode($message, true);
    }
}