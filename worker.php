<?php

require_once 'vendor/autoload.php';

$_SERVER['DOCUMENT_ROOT'] = $_SERVER['DOCUMENT_ROOT'] ?: dirname(__FILE__);

(new \App\kernel\Worker())->listen('task_queue');