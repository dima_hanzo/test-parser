
## Install

- run `composer install`
- `cd config` 
- `cd cp app.php.example app.php`
- `cd cp db.php.example db.php`
- `cd cp rabbitmq.php.example rabbitmq.php`
- setup db connection in `config/db.php`
- setup rabbit connection in `config/db.php`
- setup constants `FORUMODUA_LOGIN` and `FORUMODUA_PASSWORD` in `config/app.php`
 also u can setup link and limit
- run `php migration.php`
- run `php worker.php`
- run `php parser.php`
