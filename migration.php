<?php
require_once 'vendor/autoload.php';

$_SERVER['DOCUMENT_ROOT'] = $_SERVER['DOCUMENT_ROOT'] ?: dirname(__FILE__);

use App\database\Connection;

const CREATE_TABLE = "create table posts
(
    id     serial primary key,
    title  varchar,
    author varchar,
    date   timestamp,
    text   text
);";

const CREATE_PROCEDURE = "CREATE PROCEDURE insertPost(_title varchar,
                          _author varchar,
                          _date timestamp,
                          _text text)
    LANGUAGE SQL
AS
$$
INSERT INTO posts(title,
                  author,
                  date,
                  text)
VALUES (_title,
        _author,
        _date,
        _text);
$$;";

/** @var PDO $db */
$db = Connection::getInstance()->db();
if ($db->query(CREATE_TABLE)) {
    echo "Table created! \n";
} else {
    echo "Table didn't created! \n";
}
if ($db->query(CREATE_PROCEDURE)) {
    echo "Procedure created! \n";
} else {
    echo "Procedure didn't created! \n";
}
