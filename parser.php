<?php
require_once 'vendor/autoload.php';

$_SERVER['DOCUMENT_ROOT'] = $_SERVER['DOCUMENT_ROOT'] ?: dirname(__FILE__);

use App\kernel\Config;
use App\parser\ForumOdUaParser;
use App\parser\SiteParser;

function run(SiteParser $connector, ?string $link)
{
    $config = Config::getInstance();
    $link = $link ?? $config->getValue("app.sites.forumodua.parser");
    $connector->parse($link);
}

run(new ForumOdUaParser(), Config::getInstance()->getValue("app.sites.forumodua.link"));





